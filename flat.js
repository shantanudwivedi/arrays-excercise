let myReduce = require('./reduce.js');


function myFlat(array, depth = 0) {
    if (depth < 1 || !Array.isArray(array)) {
        return array;
    }
    return myReduce(
        array,
        (result, current) => {
            return result.concat(myFlat(current, depth - 1));
        },
        [],
    );
}

module.exports = myFlat;