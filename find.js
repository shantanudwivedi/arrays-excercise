let findIndex = require("./findIndex.js")


function myFind(array, callback) {
    const index = findIndex(array, callback);
    if (index === -1) {
        return undefined;
    }
    return array[index];
}

module.exports = myFind;